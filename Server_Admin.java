//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import java.util.*;
import java.io.*;
import socketio.*;

public class Server_Admin implements Runnable {
  
  Lotto_Server server;
  Scanner scan = new Scanner(System.in);
  
  public Server_Admin(Lotto_Server server) {
    this.server = server;
  }  
  
  public void run() {
    while (true) { 
      try {
        String eingabe = scan.nextLine();
        
        if (eingabe.equals("/help")) {
          System.out.println("\nAdmin-Commands:\n/run\t\t\tStartet Ziehung\n/setTime HH:MM\t\tZeit f�r Ziehung\n/register us pw\t\tRegistriert neuen Benutzer");
          
        } else if (eingabe.contains("/setTime") || eingabe.contains("/settime")) {
          //Set Time
          for (Server_Handler sv : server.handler) {
            sv.client.write(eingabe + "\n");
            server.timehandler.setTime(eingabe.split(" ")[1]);
            System.out.println("Zeit wurde eingestellt");
          } // end of for
        }
        
        else if (eingabe.contains("/run")){
          System.out.println("Ziehung wird gestartet");
          server.starteZiehung();
          System.out.println("");
        }// end of if-else
        
        else if (eingabe.contains("/setmoney")){
          for (Server_Handler sh : server.handler) {
            if (sh.username.equals(eingabe.split(" ")[1])) {
              sh.client.write(eingabe + "\n");
              sh.konto = Double.parseDouble(eingabe.split(" ")[2]);
              System.out.println("Kontostand wurde aktualisiert");
              return;
            }
          } // end of for
          System.out.println("Benutzer konnte nicht gefunden werden");
        }// end of if-else
        
        else if (eingabe.contains("/register")){
          FileWriter fw = new FileWriter(new File("Lotto_Server_Users.txt"), true);
          fw.write("_" + eingabe.split(" ")[1] + ":" + eingabe.split(" ")[2] + ":15");
          fw.close();
          System.out.println("Benutzer '" + eingabe.split(" ")[1] + "' wurde erfolgreich angelegt.");
        }// end of if-else
        
        else {
          System.out.println("Admin: Dieser Befehl existiert nicht");
        } // end of if-else
      } catch(Exception e) {
        System.out.println("Error-Admin: " + e.getMessage());
      }
    } // end of while
  }
          
} // end of class Server_Admin
          
