//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;
import java.io.*;
import java.util.*;
import socketio.*;

public class Lotto_Client_Main implements EventHandler {
  
  //JavaFX Elemente
  int sizeX = 550;
  int sizeY = 524;
  
  Rectangle[] cbs = new Rectangle[49];     
  Text[] txs = new Text[49];
  Line[] cr1 = new Line[49];
  Line[] cr2 = new Line[49];
  Button bsenden, bquicktipp;
  Text lbl3, lbl4, lblTime, lblMoney;
  Text[] akLbl = new Text[7];
  int anzahl = 0;
  int anzahlscheine = 0;
  Accordion[] accordion = new Accordion[7];
  ArrayList<int[]> zahlen = new ArrayList<int[]>();
  double konto = 0.0;
  
  //Communication
  Socket client;
  Lotto_Client_Time timehandler;
  String username = "unbekannt";
  public Lotto_Client_Login mutterklasse;

    
  public Lotto_Client_Main(Socket client, String username, Lotto_Client_Login mutterklasse) {
    this.client = client;
    this.username = username;
    this.mutterklasse = mutterklasse;
    
    //Time
    timehandler = new Lotto_Client_Time(this, 0, 0);
    Thread th = new Thread(timehandler);
    th.start();
    
    //Handler
    Thread tt = new Thread(new Lotto_Client_Main_Handler(this, client));
    tt.start();
  }
  
  public Scene startScene() throws Exception {
    Group group = new Group();
    
    Rectangle rect = new Rectangle();
    rect.setWidth(sizeX+50);
    rect.setHeight(80);
    rect.setFill(Color.YELLOW);
    group.getChildren().add(rect);
    
    Text lbl1 = new Text("LOTTO 6      49");
    lbl1.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 45));
    lbl1.setFill(Color.RED);
    lbl1.setLayoutX(40);
    lbl1.setLayoutY(60);
    group.getChildren().add(lbl1);
    
    Text lbl2 = new Text("aus");
    lbl2.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 30));
    lbl2.setFill(Color.RED);
    lbl2.setLayoutX(270);
    lbl2.setLayoutY(60);
    group.getChildren().add(lbl2);
    
    lbl3 = new Text("Kreuzen Sie Ihre 6 Wunschzahlen an.");
    lbl3.setFont(Font.font("Arial", 18));
    lbl3.setFill(Color.BLACK);
    lbl3.setLayoutX(10);
    lbl3.setLayoutY(113);
    group.getChildren().add(lbl3);
    
    lblTime = new Text("N�chste Ziehung:\nunbekannt");
    lblTime.setFont(Font.font("Arial", 15));
    lblTime.setFill(Color.RED);
    lblTime.setLayoutX(425);
    lblTime.setLayoutY(40);
    group.getChildren().add(lblTime);
    
    lblMoney = new Text("Konto:\t"+ konto +" �");
    lblMoney.setFont(Font.font("Arial", 15));
    lblMoney.setFill(Color.BLACK);
    lblMoney.setLayoutX(425);
    lblMoney.setLayoutY(100);
    group.getChildren().add(lblMoney);
    
    bsenden = new Button("Zahlen absenden");
    bsenden.setFont(Font.font("Arial", 15));
    bsenden.setLayoutX(105);
    bsenden.setLayoutY(480);
    bsenden.setOnMouseClicked(this);
    bsenden.setDisable(true);
    bsenden.setMinSize(248, 30);
    group.getChildren().add(bsenden);
    
    bquicktipp = new Button("QuickTipp");
    bquicktipp.setFont(Font.font("Arial", 15));
    bquicktipp.setLayoutX(10);
    bquicktipp.setLayoutY(480);
    bquicktipp.setMinSize(40, 30);
    bquicktipp.setOnMouseClicked(this);
    group.getChildren().add(bquicktipp);
    
    lbl4 = new Text("Ihre Lottoscheine: \nKeine Eintr�ge");
    lbl4.setFont(Font.font("Arial", 19));
    lbl4.setFill(Color.BLACK);
    lbl4.setLayoutX(360);
    lbl4.setLayoutY(143);
    group.getChildren().add(lbl4);
    
    //Akkordiomn
    int ssY = 150;
    TitledPane pane1;
    for (int i = 0;i< akLbl.length ;i++ ) {
      akLbl[i] = new Text("");
      accordion[i] = new Accordion();
      pane1 = new TitledPane((i+1) + ". Lottoschein" , akLbl[i]);
      accordion[i].setLayoutX(360);
      accordion[i].setLayoutY(ssY);
      accordion[i].setMinWidth(sizeY-360-10);
      accordion[i].setVisible(false);
      pane1.setFont(Font.font("Arial", 20));
      accordion[i].getPanes().add(pane1);
      group.getChildren().add(accordion[i]);
      ssY += 53;
    } // end of for
    
    
    //Checkboxen
    int cbs_size = 40;
    int cbs_startx = 10;
    int cbs_starty = 130;
    int cx = cbs_startx;
    int cy = cbs_starty;
    for (int i = 0; i < cbs.length; i++) { 
      cbs[i] = new Rectangle();
      cbs[i].setWidth(cbs_size);
      cbs[i].setHeight(cbs_size);
      cbs[i].setLayoutX(cx);
      cbs[i].setLayoutY(cy);
      cbs[i].setFill(Color.WHITE);
      cbs[i].setStroke(Color.RED);
      cbs[i].setOnMouseClicked(this);
      group.getChildren().add(cbs[i]);
      
      txs[i] = new Text(Integer.toString((i+1)));
      txs[i].setFill(Color.RED);
      txs[i].setFont(Font.font("Arial", 20));
      txs[i].setOnMouseClicked(this);
      if ((i+1) >= 10) txs[i].setLayoutX(cx+9); else txs[i].setLayoutX(cx+14);
      txs[i].setLayoutY(cy+27);
      group.getChildren().add(txs[i]);
      
      cx = cx + cbs_size + 10;
      if (cx > 350) {
        cx = cbs_startx;
        cy = cy + cbs_size + 10;
      }
    } // end of for
    
    //Lines (Kreuze)
    int buffer = 5;
    for (int i = 0; i < cr1.length; i++) {
      cr1[i] = new Line();
      cr1[i].setStartX(cbs[i].getLayoutX()+buffer);
      cr1[i].setStartY(cbs[i].getLayoutY()+buffer);
      cr1[i].setEndX(cbs[i].getLayoutX()+cbs_size-buffer);
      cr1[i].setEndY(cbs[i].getLayoutY()+cbs_size-buffer);
      cr1[i].setStrokeWidth(3);
      cr1[i].setVisible(false);  
      cr1[i].setOnMouseClicked(this);
      group.getChildren().add(cr1[i]);
      cr2[i] = new Line();
      cr2[i].setStartX(cbs[i].getLayoutX()+buffer);
      cr2[i].setStartY(cbs[i].getLayoutY()+cbs_size-buffer);
      cr2[i].setEndX(cbs[i].getLayoutX()+cbs_size-buffer);
      cr2[i].setEndY(cbs[i].getLayoutY()+buffer);
      cr2[i].setStrokeWidth(3);
      cr2[i].setVisible(false);
      cr2[i].setOnMouseClicked(this);
      group.getChildren().add(cr2[i]);
    }
    
    return new Scene(group, sizeX, sizeY);
  }
  

  public void handle(Event ev) {
    if (konto < 1.50) {
      bsenden.setDisable(true);
      return;
    }
    //Felder
    for (int i = 0; i<cbs.length ;i++ ) {
      if (ev.getSource() == cbs[i] || ev.getSource() == txs[i] || ev.getSource() == cr1[i] ||ev.getSource() == cr2[i]) {
        if (anzahlscheine >= 7) {
          bquicktipp.setDisable(true);
          bsenden.setDisable(true);
          return;
        }
        //Feld angeklickt
        if (!cr1[i].isVisible() && anzahl < 6) {
          cr1[i].setVisible(true);
          cr2[i].setVisible(true);
          anzahl++;
          if (anzahl != 6) lbl3.setText("Kreuzen Sie Ihre weiteren "+ (6-anzahl) + " Wunschzahlen an.");
          if (anzahl == 0) lbl3.setText("Kreuzen Sie Ihre 6 Wunschzahlen an.");
          if (anzahl == 6) lbl3.setText("Anzahl von Wunschzahlen erreicht.");
          if (anzahl == 6) bsenden.setDisable(false);
          return;
        }
        if (cr1[i].isVisible()) {
          cr1[i].setVisible(false);
          cr2[i].setVisible(false);
          anzahl--;
          if (anzahl != 6) { lbl3.setText("Kreuzen Sie Ihre weiteren "+ (6-anzahl) + " Wunschzahlen an."); bsenden.setDisable(true); }
          if (anzahl == 0) lbl3.setText("Kreuzen Sie Ihre 6 Wunschzahlen an.");
          if (anzahl == 6) lbl3.setText("Anzahl von Wunschzahlen erreicht.");
          if (anzahl == 6) bsenden.setDisable(false);
          return;
        }
      } 
    } // end of for
    
    
    //QuickTipp (Zufallszahlen)
    if (ev.getSource() == bquicktipp) {
      if (anzahlscheine >= 7) {
        bquicktipp.setDisable(true);
        bsenden.setDisable(true);
        return;
      }
      for (int i = 0;i < cr1.length ;i++ ) {
        cr1[i].setVisible(false);
        cr2[i].setVisible(false);
      } // end of for
      for (int i = 0; i<6 ;i++ ) {
        int zufallszahl = (int) (Math.random() * 49);
        if (cr1[zufallszahl].isVisible()) i--;
        cr1[zufallszahl].setVisible(true);
        cr2[zufallszahl].setVisible(true);
      } // end of for
      anzahl = 6;
      bsenden.setDisable(false);
      lbl3.setText("Anzahl von Wunschzahlen erreicht.");
    }
    
    //Zahlen senden
    if (ev.getSource() == bsenden) {
      bsenden.setDisable(true);
      if (anzahlscheine >= 7) {
        bquicktipp.setDisable(true);
        return;
      }
      try {
        int[] z = new int[7];
        int pos = 0;
        for (int i = 0;i<cr1.length ;i++ ) {
          if (cr1[i].isVisible()) {
            cr1[i].setVisible(false);
            cr2[i].setVisible(false);
            anzahl = 0;
            z[pos] = (i+1);
            pos++;
          } 
        } // end of for
        Arrays.sort(z);
        zahlen.add(z);
        for (int i = 0;i<6 ;i++ ) {
          akLbl[anzahlscheine].setText(akLbl[anzahlscheine].getText() + Integer.toString(zahlen.get(anzahlscheine)[i]));
          if (i != 5) akLbl[anzahlscheine].setText(akLbl[anzahlscheine].getText() + ", ");
        } // end of for
        accordion[anzahlscheine].setVisible(true);
        
        //Senden
        client.write(username + ":" + zahlen.get(anzahlscheine)[0] + ":"+ zahlen.get(anzahlscheine)[1] + ":"+ zahlen.get(anzahlscheine)[2] + ":"+ zahlen.get(anzahlscheine)[3] + ":"+ zahlen.get(anzahlscheine)[4] + ":"+ zahlen.get(anzahlscheine)[5] + "\n");
        
        anzahlscheine++;  
      } catch(Exception e) {
        System.out.println("F Zahlen senden: " + e.getMessage());
      } 
    }
  }
  
  

} // end of class Lotto_Client_Main

