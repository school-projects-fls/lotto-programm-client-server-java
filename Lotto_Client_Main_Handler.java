//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import java.util.*;
import java.io.*;
import socketio.*;

public class Lotto_Client_Main_Handler implements Runnable {
  
  Lotto_Client_Main server;
  Socket client;
  
  public Lotto_Client_Main_Handler (Lotto_Client_Main server, Socket client) {
    this.server = server;
    this.client = client;
  }
  
  public void run() {
    while (true) { 
      try {
        while (client.dataAvailable() <= 0);
        String empfang = client.readLine();
        System.out.println("Empfangen: " + empfang);
        
        //Empfang abfragen ob Zeit oder anderes
        if (empfang.contains("/setTime") || empfang.contains("/settime")) server.timehandler.setTime(empfang.split(" ")[1]);
        
        if (empfang.contains("/setmoney")) {
          server.konto = Double.parseDouble(empfang.split(" ")[2]); 
          server.lblMoney.setText("Konto:\t"+ server.konto +" �");
        }
        
        ArrayList<String> strs = new ArrayList<String>();
        if (empfang.equals("Ziehung")) {  
          for (int i = 0; i < 8; i++) {
            while (client.dataAvailable() < 0);
            empfang = client.readLine();
            System.out.println(empfang);
            if (empfang.equals("Ende")) break;
            strs.add(empfang);
          }
          System.out.println("warten");
          while (client.dataAvailable() <= 0);
          String k = client.readLine();
          double konto = Double.parseDouble(k);
          server.mutterklasse.showResultWindow(strs, konto);
        }
      } catch(Exception e) {
        System.out.println("F. " + e.getMessage()); 
      }
    } // end of while
  }
  
} // end of class Lotto_Client_Main_Handler

