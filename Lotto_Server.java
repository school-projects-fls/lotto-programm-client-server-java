//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import socketio.*;
import java.io.*;
import java.util.*;

public class Lotto_Server {
  
  ServerSocket server;
  Socket client;
  boolean offen = true;
  Server_Time timehandler;
  
  int zahlen[] = {-1, -1, -1, -1, -1, -1};
  ArrayList<Server_Handler> handler;
  
  public Lotto_Server() {
    try {
      server = new ServerSocket(1234);
      System.out.println("Server wurde erstellt");
      Thread t = new Thread(new Server_Admin(this));
      t.start();
      
      handler = new ArrayList<Server_Handler>();
      timehandler = new Server_Time(this);
      Thread ttt = new Thread(timehandler);
      ttt.start();
      
      while (offen) { 
        client = server.accept();
        System.out.println("Benutzer 'unbekannt' verbindet sich");
        Server_Handler sh = new Server_Handler(this, client, (handler.size()));
        handler.add(sh);
        Thread th = new Thread(sh);
        th.start();
      } // end of while
      
    } catch(Exception e) {
      System.out.println("Error-Main: " + e.getMessage());
    }
  }
  
  public void starteZiehung() {
    for (int i = 0;i<6 ;i++ ) {
      int zufallszahl = (int) (Math.random() * 49) +1;
      zahlen[i] = zufallszahl;
      for (int a = 0;a<6 ;a++ ) {
        if (a != i && zahlen[a] == zufallszahl) {
          i--;
          break;
        }
      } // end of for
    } // end of for
    Arrays.sort(zahlen);
    System.out.println("Ziehung ergab folgende Zahlen: " + zahlen[0] + ", " + zahlen[1] + ", " + zahlen[2] + ", " + zahlen[3] + ", " + zahlen[4] + ", " + zahlen[5]);
    
    for (Server_Handler h : handler) {
      h.checkeZahlen(zahlen);
    } // end of for
  }
  
  public static void main(String[] args) {
    Lotto_Server ls = new Lotto_Server();
  } // end of main

} // end of class Lotto_Server

