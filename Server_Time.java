//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06

import java.text.*;
import java.time.*;
import java.util.*;

public class Server_Time implements Runnable{
  
  Lotto_Server server;
  int gghour, ggmin;
  int ahour, amin;
  
  public Server_Time(Lotto_Server server) {
    this.server = server;
  }
  
  public void setTime(String temp) {
    gghour = Integer.parseInt(temp.split(":")[0]);
    ggmin = Integer.parseInt(temp.split(":")[1]);
  }
  
  public void run() {
    
    while (true) { 
      Date d = new Date();
      int ahour = Integer.parseInt(d.toString().split(" ")[3].split(":")[0]);
      int amin = Integer.parseInt(d.toString().split(" ")[3].split(":")[1]);
      
      if (ahour == gghour && amin == ggmin) {
        System.out.println("Zeit wurde erkannt");
        server.starteZiehung();
        return;
      }
      
      try {
        Thread.sleep(1000);
      } catch(Exception e) {
        System.out.println("Error-Time:" + e.getMessage());
      } 
      
    } // end of while
    
  }
      
} // end of class Lotto_Client_Time
      
