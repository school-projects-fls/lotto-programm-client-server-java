//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06

import java.text.*;
import java.time.*;
import java.util.*;

public class Lotto_Client_Time implements Runnable{
  
  Lotto_Client_Main server;
  int gghour, ggmin;
  int ahour, amin;
  
  public Lotto_Client_Time(Lotto_Client_Main server, int ghour, int gmin) {
    this.server = server;
    this.gghour = ghour;
    this.ggmin = gmin;
  }
  
  public void setTime(String temp) {
    gghour = Integer.parseInt(temp.split(":")[0]);
    ggmin = Integer.parseInt(temp.split(":")[1]);
  }
  
  public void run() {
    
    while (true) { 
      int ghour = gghour;
      int gmin = ggmin;
      
      int timedif = 0;
      Date d = new Date();
      int ahour = Integer.parseInt(d.toString().split(" ")[3].split(":")[0]);
      int amin = Integer.parseInt(d.toString().split(" ")[3].split(":")[1]);
      
      if (gmin > amin) timedif += gmin - amin;
      else { timedif += 60-(amin-gmin); ghour--; }
      if (ghour >= ahour) timedif += (ghour-ahour)*60;
      
      if (gghour == 0 && ggmin == 0) server.lblTime.setText("N�chste Ziehung:\nunbekannt"); else if (timedif == 1) server.lblTime.setText("N�chste Ziehung\nin " + timedif + " Minute"); else server.lblTime.setText("N�chste Ziehung\nin " + timedif + " Minuten");
      try {
        Thread.sleep(1000);
      } catch(Exception e) {
        System.out.println(e.getMessage());
      } 
      
    } // end of while
    
  }
      
} // end of class Lotto_Client_Time
      
