//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.shape.*;                        //Zeit testen und (Minuten/Stunden �berzug)
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;
import java.io.*;
import socketio.*;
import java.util.*;

public class Lotto_Client_Login extends Application implements EventHandler {
  boolean skiplogin = false;
  boolean skiptoresult = false;
  
  //JavaFX Elemente
  Stage window;
  int sizeX = 500;
  int sizeY = 200;
  Text lbl6;
  Button btn1;
  TextField txf1; PasswordField txf2;
  
  //Communication
  Socket client;
  boolean login_accepted = false;
  boolean result = false;
  
  public static void main(String[] args) {
    launch(args);
  } // end of main
  
  public void start(Stage windowa) throws Exception {
    Group group = new Group();
    this.window = windowa;
    
    Rectangle rect = new Rectangle();
    rect.setWidth(sizeX+50);
    rect.setHeight(80);
    rect.setFill(Color.YELLOW);
    group.getChildren().add(rect);
    
    Text lbl1 = new Text("LOTTO 6      49");
    lbl1.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 45));
    lbl1.setFill(Color.RED);
    lbl1.setLayoutX(40);
    lbl1.setLayoutY(60);
    group.getChildren().add(lbl1);
    
    Text lbl2 = new Text("aus");
    lbl2.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 30));
    lbl2.setFill(Color.RED);
    lbl2.setLayoutX(270);
    lbl2.setLayoutY(60);
    group.getChildren().add(lbl2);
    
    Text lbl3 = new Text("Melden Sie sich mit Ihrem Account ein.");
    lbl3.setFont(Font.font("Verdana", 12));
    lbl3.setFill(Color.BLACK);
    lbl3.setLayoutX(30);
    lbl3.setLayoutY(105);
    group.getChildren().add(lbl3);  
    
    //Login
    Text lbl4 = new Text("Benutzername:");
    lbl4.setFont(Font.font("Verdana", 14));
    lbl4.setFill(Color.BLACK);
    lbl4.setLayoutX(30);
    lbl4.setLayoutY(140);
    group.getChildren().add(lbl4);
    
    Text lbl5 = new Text("Passwort:");
    lbl5.setFont(Font.font("Verdana", 14));
    lbl5.setFill(Color.BLACK);
    lbl5.setLayoutX(230);
    lbl5.setLayoutY(140);
    group.getChildren().add(lbl5);
    
    lbl6 = new Text("");
    lbl6.setFont(Font.font("Verdana", 12));
    lbl6.setFill(Color.RED);
    lbl6.setLayoutX(30);
    lbl6.setLayoutY(195);
    //lbl6.setVisible(false);
    group.getChildren().add(lbl6);
    
    txf1 = new TextField();
    txf1.setLayoutX(30);
    txf1.setLayoutY(150);
    group.getChildren().add(txf1);
    
    txf2 = new PasswordField();
    txf2.setLayoutX(230);
    txf2.setLayoutY(150);
    group.getChildren().add(txf2);
    
    btn1 = new Button("Login");
    btn1.setLayoutX(400);
    btn1.setLayoutY(150);
    btn1.setMinWidth(75);
    btn1.setMinHeight(25);
    btn1.setOnAction(this);
    group.getChildren().add(btn1);
    
    //Stage Einstellungen
    Scene scene = new Scene(group, sizeX, sizeY);
    window.setScene(scene);
    window.setResizable(false);
    window.setTitle("Lotto Client Login");
    window.show();
    
    //DEV: Skiplogin
    if (skiplogin) {
      try {
        client = new Socket("127.0.0.1", 1234);
        client.connect();
      } catch(Exception e) {
        System.out.println(e.getMessage()); 
      }
      txf1.setText("unbekannt");
      showMainWindow();
    }
    if(skiptoresult) showResultWindow(null, 0);
  }
  
  
  public void handle(Event arg0) {
    //ButtonListener
    if (arg0.getSource() == btn1) {
      btn1.setDisable(true);
      lbl6.setText("");
      if (txf1.getText().equals("") || txf1.getText().equals("")) { lbl6.setText("Nicht alle Felder ausgef�llt!"); btn1.setDisable(false); return; }
      connect();
      btn1.setDisable(false);
    }
  }
    
  public void connect() {
    try {
      
      //Config Datei auslesen
      FileReader datei = new FileReader("Lotto_Client_CFG.txt");
      String inhalt = "";
      while (datei.ready()) { 
        inhalt = inhalt + (char) datei.read();
      } // end of while
      datei.close();
      
      //Verbinden
      client = new Socket(inhalt.split(":")[0], Integer.parseInt(inhalt.split(":")[1]));
      if (client.connect()) { System.out.println("Verbunden zu Server: " + inhalt.split(":")[0] + " Port: " + inhalt.split(":")[1]);} else {
        lbl6.setText("Server konnte nicht gefunden werden.");
        return;
      }
      
      //Account-Abfrage
      Thread.sleep(1000);
      client.write(txf1.getText() + "\n");
      System.out.println("ges1");
      Thread.sleep(1000);
      client.write(txf2.getText() + "\n");    
      System.out.println("ges"); 
      
      //Antwort
      while (client.dataAvailable() < 0);
      System.out.println("weiter");
      if (client.readLine().equals("erfolgreich")) {
        lbl6.setText("Anmeldung erfolgreich");
        showMainWindow(); 
        //showResultWindow();
      } else {
        lbl6.setText("Benutzername oder Passwort sind falsch.");
        txf1.clear();
        txf2.clear();
      } // end of if-else
    } catch(Exception e) {
      System.out.println(e.getMessage());
    } 
  }

  
  public void showMainWindow() {
    try {
      Lotto_Client_Main lc_main = new Lotto_Client_Main(client, txf1.getText(), this);
      window.setScene(lc_main.startScene());
      window.show();
      
    } catch(Exception e) {
      System.out.println("F1" + e.getMessage());
    } 
  }
  
  //geht nicht
  public void showResultWindow(ArrayList<String> strs, double konto) {
    System.out.println("ShowResultWindow");
    try {
      Platform.runLater(()->{
        try {
          Lotto_Client_Result lc_result = new Lotto_Client_Result(this, strs, konto);
          window.setScene(lc_result.startScene());
          window.show();
        } catch(Exception e) {
          System.out.println(e.getMessage());
        } 
      });
    } catch(Exception e) {
      System.out.println("F1" + e.getMessage());
    }  
  }
    
} // end of class Lotto_Client    
