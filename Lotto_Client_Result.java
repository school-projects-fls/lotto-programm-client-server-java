//Lotto Programm by Nils Trautner & Leon Micheel 03.06.2018 TEWI 12/06
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;
import java.io.*;
import java.util.*;
import socketio.*;

public class Lotto_Client_Result {
  
  Lotto_Client_Login mutterklasse;
  int sizeX = 500;
  int sizeY = 470;
  
  Text lbls[] = new Text[7];
  ArrayList<String> str;
  double konto;
  
  public Lotto_Client_Result(Lotto_Client_Login mutterklasse, ArrayList<String> str, double konto) {
    this.mutterklasse = mutterklasse;
    this.str = str;
    this.konto = konto;
  }
  
  public Scene startScene() throws Exception {
    Group group = new Group();
    
    Rectangle rect = new Rectangle();
    rect.setWidth(sizeX+50);
    rect.setHeight(80);
    rect.setFill(Color.YELLOW);
    group.getChildren().add(rect);
    
    Text lbl1 = new Text("LOTTO 6      49");
    lbl1.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 45));
    lbl1.setFill(Color.RED);
    lbl1.setLayoutX(40);
    lbl1.setLayoutY(60);
    group.getChildren().add(lbl1);
    
    Text lbl2 = new Text("aus");
    lbl2.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 30));
    lbl2.setFill(Color.RED);
    lbl2.setLayoutX(270);
    lbl2.setLayoutY(60);
    group.getChildren().add(lbl2);
    
    Text lbl3 = new Text("Ziehung von " + new Date().toString());
    lbl3.setFont(Font.font("Verdana", 15));
    lbl3.setFill(Color.BLACK);
    lbl3.setLayoutX(15);
    lbl3.setLayoutY(115);
    group.getChildren().add(lbl3);
    
    Text lbl4 = new Text("Die gezogenen Lottozahlen:");
    lbl4.setFont(Font.font("Verdana", 20));
    lbl4.setFill(Color.BLACK);
    lbl4.setLayoutX(15);
    lbl4.setLayoutY(160);
    group.getChildren().add(lbl4);
    
    for (int i = 0; i<str.size(); i++) {
      lbls[i] = new Text((i+1) + ". Schein: " + str.get(i).split(";")[6] + " richtige Zahlen\t\tGewinn: " + str.get(i).split(";")[8] + " �");
      lbls[i].setFont(Font.font("Verdana",  15));
      lbls[i].setFill(Color.BLACK);
      lbls[i].setLayoutX(15);
      lbls[i].setLayoutY(250+i*30);
      group.getChildren().add(lbls[i]);
    } // end of for
    
    Text lbl5 = new Text("Neuer Kontostand: " + konto + " �");
    lbl5.setFont(Font.font("Verdana", 15));
    lbl5.setFill(Color.BLACK);
    lbl5.setLayoutX(15);
    lbl5.setLayoutY(460);
    group.getChildren().add(lbl5);
    System.out.println(konto);
    
    int cbs_size = 40;
    int cx = 15;
    int cy = 175;
    Rectangle cbs;
    Text txs;
    for (int i = 0;i<6 ;i++ ) {
      cbs = new Rectangle();
      cbs.setWidth(cbs_size);
      cbs.setHeight(cbs_size);
      cbs.setLayoutX(cx);
      cbs.setLayoutY(cy);
      cbs.setFill(Color.WHITE);
      cbs.setStroke(Color.RED);
      group.getChildren().add(cbs);
      
      txs = new Text(str.get(0).split(";")[i]);
      txs.setFill(Color.RED);
      txs.setFont(Font.font("Arial", 20));
      if (Integer.parseInt(str.get(0).split(";")[i]) >= 10) txs.setLayoutX(cx+9); else txs.setLayoutX(cx+14);
      txs.setLayoutY(cy+27);
      group.getChildren().add(txs);
      
      cx += 50;
    } // end of for
    
    //HIER DIE EINZELNEN SCHEINE MALEN WIE OBEN DIE CBS, MUSS NICHT GESPEICHERT WERDEN; ARRaYLIST MIT int[] oben bereits angelegt, testweise zahlen anlegen, sp�ter verkn�pfen
    
    return new Scene(group, sizeX, sizeY);
  }

} // end of class Lotto_Client_Result

